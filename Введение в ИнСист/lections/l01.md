### Лекция 1. Общее представление о восприятии человеком окружающего мира

**Определение.** *Искусственный интеллект* - это направление науки и техники, ориентированное на создание программно-аппаратных средств решения интеллектуальных задач.

**Примеры** задач, в которых *логическая* обработка *превалирует* над *вычислительной*:
* анализ ситуаций и принятие решений
* понимание и синтез текстов на естественном языке
* понимание и синтез речи
* управление роботами
* анализ визуальной информации

**Утверждение.** Для *моделирования человеческих рассуждений* используются модели логико-символьного уровня:
* логические (формальные)
* эвристические (неформальные)

**Определение.** *Эвристики* - теоретически необоснованные правила. Чаще всего записываются в символьном виде.

**Утверждение.** Для *моделирования процессов обработки* образной информации используются другие модели:
* нейросети
* генетические алгоритмы

**Определение.** *Предметной областью* называется фрагмент окружающего мира, являющегося сущностями и отношениями между ними.

**Определение.** *Проблемной областью* называется предметная область плюс решаемые задачи.

**Определение.** *Гносеология* - теория познания (раздел философии), занимается проблемами, связанными с тем, как человек понимает и отражает окружающий мир.

**Определение.** *Понятийная структура* - совокупность понятий предметной области плюс способы рассуждений на понятиях, связанные с решением конкретных задач.

**Определение.** *Когнитология* (инженерия знаний) - наука, являющаяся одним из разделов ИИ, решающая задачи передачи сформированной понятийной структуры от человека в компьютер.

**Утверждение.** В ИИ для моделирования понятийной структуры проблемной области используются три *источника знаний*:
1. *Первого рода* - человек
1. *Второго рода* - книги, справочники, инструкции
1. *Третьего рода* - базы данных, электронные носители

**Определение.** *Концептуализация* - перенос понятийной структуры в компьютер. Например, в объектную модель.

**Определение.** *Модель проблемной области* - база знаний, которая записывается в компьютер на языках высокого уровня, обычно приближенных к естественным. **Замечание:** *основная задача инженера по знаниям* связана с построением модели проблемной области.

**Утверждение.** Современные методы ИИ ориентируются в основном, на *вербальные* способы переноса информации в компьютер.

**Утверждение.** Любая система, обладающая базой знаний о некоторой проблемной области, может рассматриваться как интеллектуальная.

**Определение.** *Онтология* - эксплицитная спецификация концептуализации (по Т.Губеру).

**Определение.** *Модель онтологии* - это тройка вида $`O=<A,B,C>`$, где
* $`A`$ - конечное множество концептов (понятий, терминов) предметной области, которую представляет собой онтология $`O`$.
* $`B`$ - конечное множество отношений между концептами заданной предметной области.
* $`C`$ - конечное множество функций интерпретации, заданных на $`A`$ и $`B`$.

**Определение.** *Онтологии* - это базы знаний специального типа, которые могут "читаться", "пониматься", "отчуждаться" от их разработчика или физически разделяться пользователями.

**Замечание.** Так появилась новая ветвь инженерии знаний: *онтологический инжиниринг* - как эволюционное развитие инженерии знаний.

**Замечание.** Современные *многоагентные системы* используют онтологии и агентно-ориентированные парадигмы, базирующиеся на *агентах*.

**Утверждение.** По мнению Рассела и Норвига, *идея интеллектуального агента* является сегодня главной объединяющей темой, и ИИ определяется ими как наука об агентах, которые получают результаты актов восприятия их своей среды и выполняют действия, причём каждый такой агент реализует функцию, которая отображает последовательности актов восприятия в действии.

**Замечание.** В настоящее время не существует универсального определения искусственного интеллекта, оно формулируется в основном исходя из конкретных целей и задач.