### 12. Возможные подходы к организации коллектива разработчиков: роли, права, ответственности.

Функции, выполняемые разработчиками, - понятие неформализованное. В разных проектах оно может обретать свое содержание.

Тем не менее существуют типовые функции практически во всех программных проектах.

В любом программном проекте есть функции кодирования, т.е. записи программы на алгоритмическом языке по имеющимся спецификациям, анализа требований, т.е. выявления истинной потребности в создаваемой программе, тестирования и отладки. В рамках деятельности менеджера любого проекта необходимо организовать распределение функций проекта между исполнителями.
В результате ее выполнения члены команды, выполняющей проект, присупают к исполнению соответствующих ролей.

Понятно, что как состав, так и значимость ролей разработчиков и тех, кто с ними связан, различаются в зависимости от выполняемого проекта, от коллектива исполнителей, принятой технологии, от других факторов. Неоднозначность выбора ролей приводит к тому, что их список часто становится слишком большим (иллюстрацией тому может служить первая редакция документации по Rational Unified Processing (RUP), в которой определено свыше 20 ролей). Это является следствием отождествления роли и функции и, соответственно, игнорирования понятия родственности функций. В то же время, если ролей выбрано недостаточно, есть опасность, что не все нужные в проекте функции будут охвачены планированием и управлением.

**Модель SCRUM**

В соответствии с данной методологией управления проектами ответственность за результат делится между тремя ролями:

* Владелец продукта – определяет проектные цели, разрабатывает оптимальный график при заданных проектных параметрах, адаптирует процесс выполнения проекта к изменившимся требованиям и устанавливает приоритеты в характеристиках продукта.

* Scrum мастер – устанавливает приоритеты в выполнении задач командой проекта и устраняет возникающие затруднения, препятствующие этому.

* Члены команды – выполняют большинство поставленных задач, осуществляют ежедневный менеджмент, создают отчеты о ходе выполнения проекта, контролируют качество продукта.

**MSF (Microsoft Solutions Framework)**

В зависимости от размера и сложности проекта модель команд MSF допускает масштабирование. Для небольших и несложных проектов один сотрудник может выполнять несколько ролей. При этом некоторые роли нельзя объединять.

В таблице представлены рекомендации MSF относительно совмещения ролей в рамках одним членом команды. "+" означает, что совмещение возможно, "+-" - что совмещение возможно, но нежелательно, "-" означает, что совмещение не рекомендуется.

![](../images/12.png)

В частности, нельзя совмещать разработку и тестирование, поскольку необходимо, чтобы у тестировщиков был сформирован независимый взгляд на систему, базирующийся на изучении требований.

Для больших команд (более 10 человек) модель проектной группы MSF предлагает разбиение на малые многопрофильные группы направлений. Эти малые коллективы работают параллельно, регулярно синхронизируя свои усилия, каждая из которых устроена на основе модели кластеров. Такие команды имеют четко определенную задачу и ответственны за все относящиеся к ней вопросы, начиная от проектирования и составления календарного графика.
