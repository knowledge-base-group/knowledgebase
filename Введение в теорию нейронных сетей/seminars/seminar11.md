# Семинар 11

## Сеть Хопфилда

$`N`$ нейронов.
```math
w_{ii}=0, \space i=\overline{1,N}
```
$`w_{ij}=w_{ji}`$, $`i,j=\overline{1,N}`$

Полный граф без рефлексивных дуг.
Работает в асинхронном режиме. На каждом такте времени опрашивается (и, возможно, меняет состояние) только один нейрон.

Если необходимо, чтобы сеть запомнила $`P`$ образов, то
```math
x^p=(x_1^p, ...,x_N^p), \space p=\overline{1,P}
```
Синаптические коэффициенты неизменны:
```math
w_{ij} = k\sum_{p=1}^{P} x_i^p x_j^p
```

Уравнение функционирования:
```math
h_i(t) = \sum_{j=1}^{N} w_{ij}y_j(t)
```
```math
y_i(t+1)=
\begin{cases}
    sign(h_i(t)), \space h_i(t)\ne 0 \\
    y_i(t), \space h_i(t)=0
\end{cases}
```
```math
i=i(t), \space t=0,1,2,...
```
$`k`$ - это некоторая константа.

**Аттракторы сети.** Если $`w_{ij}=kx_i^1x_j^1`$, то $`x^1`$ - аттрактор, а $`-x^1`$ - тоже аттрактор (негатив).
Свойство аттрактора: если $`y(\tau)=x^1`$, то $`y(\tau+1)=y(\tau)`$.

Три вида аттракторов:
1. Примеры обучающей выборки
2. Их негативы
3. Ложная память (когда сеть вместо того, чтобы запомнить два разных образа, запоминает их комбинацию).

### 4. О сходимости сети Хопфилда

**Определение.** Энергетический функционал сети Хопфилда (функция Ляпунова).
```math
E(t) = -\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}y_i(t)y_j(t)
```

**Теорема.** (О конечности переходного процесса сети Хопфилда)
1. В асинхронном режиме функционирования $`E(t)`$ - невозрастающая функция.
2. Со временем $`E(t)`$ достигает локального минимума, причём $`E(\infty)<0`$.
3. Время данного переходного процесса является конечным.

$`\square`$ Рассмотрим пункт **1**. Прошло $`t`$ тактов времени и $`y(t)=(y_1(t),...,y_k(t),...,y_N(t))`$. Происходит опрос $`k`$-го нейрона.
```math
y_k(t+1)=y_k(t)+\varDelta y_k(t)
```
```math
E(t+1)=E(t)+\varDelta E(t)
```

Должны показать, что $`\varDelta E(t)\le 0`$.

```math
\varDelta E(t) = E(t+1)-E(t)=
-\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}y_i(t+1)y_j(t+1)
+
\sum_{i,j=1}^{N} w_{ij}y_i(t)y_j(t)=
```
```math
= -\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}
(y_i(t)+\varDelta y_i(t))(y_j(t)+\varDelta y_j(t))
+
\frac{1}{2}\sum_{i,j=1}^{N} y_i(t)y_j(t)=
```
```math
=-\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}
y_i(t)\varDelta y_j(t)-
-
\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}
\varDelta y_i(t)y_j(y)
-
\frac{1}{2}
\sum_{i,j=1}^{N} w_{ij}
\varDelta y_i(t)\varDelta y_j(t)
=
```

В то же время $`\varDelta y_i(t)\varDelta y_j(t)=0`$ в силу асинхронности.
В то же время, матрица синаптических коэффициентов симметричная: $`w_{ij}=w_{ji}`$, а также $`\varDelta y_i(t)=0`$, $`i\ne k`$.

```math
=
-
\frac{1}{2}
\sum_{i=1}^{N} w_{ik}y_i(t)\varDelta y_k(t)
-
\frac{1}{2}
\sum_{j=1}^{N} w_{kj}\varDelta y_k(t)y_j(t)=
```
```math
=
-
\frac{1}{2}
\sum_{i=1}^{N} w_{ik}\varDelta y_k(t)y_i(t)
-
\frac{1}{2}
\sum_{j=1}^{N} w_{jk}\varDelta y_k(t)y_j(t)=
```
```math
=-\sum_{j=1}^{N} w_{kj}y_j(t)\varDelta y_k(t)=
```
```math
=-h_k(y)\varDelta y_k(t)
```
Есть варианты:

а) $`y_k(t)=-1`$, $`y_k(t+1)=1`$  
$`h_k(t)>0`$ и $`\varDelta y_k(t)=2`$ $`\to`$ $`\varDelta E(t)<0`$

б) $`y_k(t)=1`$, $`y_k(t+1)=-1`$  
$`h_k(t)<0`$ и $`\varDelta y_k(t)=-2`$ $`\to`$ $`\varDelta E(t)<0`$

в) $`y_k(t)=y_k(t+1)`$
$`h_k(t)=0`$ и $`\varDelta y_k(t)=0`$ $`\to`$ $`\varDelta E(t)=0`$

Рассмотрим пункт **3**. Известно, что $`E(t+1)<E(t)`$. Покажем, что  $`E(t)`$ не может убывать бесконечно.

```math
E(t)=-\frac{1}{2} \sum_{i,j=1}^{N} w_{ij}y_i(t)y_j(t)
```
$`w_{ij}`$ - конечное значение, а $`y_i(t), \space y_j(t) \in \{-1,1\}`$

Также, не может убывать асимптотически в силу дискретности.

[рис 11.1]

Рассмотрим пункт **2**. $`y(\infty)`$ - вектор выходов нейронов в состоянии устойчивого равновесия. $`h(\infty)`$ - соответствующий вектор потенциалов.
```math
E(\infty) = -\frac{1}{2}
\sum_{i=1}^{N} \sum_{j=1}^{N} w_{ij}y_i(\infty)y_j(\infty)=
```
```math
=-\frac{1}{2}
\sum_{i=1}^{N} y_i(\infty)
\sum_{j=1}^{N} w_{ij} y_j(\infty)
```
а) $`y_i(\infty)=sign(h_i(\infty))`$, $`\to`$ $`y_i(\infty)\cdot h_i(\infty)>0`$.

б) $`h(\infty)=0`$, $`\to`$ $`y_i(\infty)\cdot h_i(\infty)=0`$. $`\blacksquare`$

### 5. Применение сети Хопфилда. Сеть Хопфилда на бинарных нейронах

Уравнение фунционирования нейрона в сети Хопфилда:
```math
h_i = \sum_{j=1}^{N} w_{ij}y_j(t)
```
```math
y_i(t+1) = 
\begin{cases}
    -1, \space h_i(t)<0 \\
    1, \space h_i(t)>0 \\
    y_i(t), \space h_i(t)=0
\end{cases}
```

Это не совсем технический нейрон. Тут третья строчка ломает всё.

Применения:
1. Ассоциативная память
2. Классификация
3. Кластеризация
4. Решение задач комбинаторной оптимизации

Рассмотрим
```math
E(t) = -\frac{1}{2} \sum_{i,j=1}^{N} w_{ij}y_i(t)y_j(t)
```

Задача комбинаторной оптимизации:
```math
\begin{cases}
    \varphi(x_1,...,x_N) \to\min \\
    x_j\in\{0,1\}, \space i=\overline{1,N}
\end{cases}
```

Тут нужно сделать переход сети Хопфилда к бинарным нейронам.

Классическая сеть:
```math
h_i = \sum_{j=1}^{N} w_{ij}y_j(t)
```
```math
y_i(t+1) = 
\begin{cases}
    -1  & h_i(t)<0 \\
    1 & h_i(t)>0 \\
    y_i(t) & h_i(t)=0
\end{cases}
```

Произведём замену переменной.
[рис 11.2]

```math
y_i(t)=2z_i(t)-1
```

Тогда
```math
h_i = \sum_{j=1}^{N} w_{ij}y_j(t) = \sum_{j=1}^{N} w_{ij}(2z_j(t)-1)=
```
```math
=2
\left(
    \sum_{j=1}^{N} w_{ij}z_j(t)
    -\frac{1}{2}\sum_{j=1}^{N} w_{ij}
\right)
```
```math
-b_i = -\frac{1}{2}\sum_{j=1}^{N} w_{ij}
```

Получили уравнение функционирования:
```math
h_i'(t)=-b_i+\sum_{j=1}^{N} w_{ij}z_j(t)
```
```math
z_i(t)=
\begin{cases}
    1 & h_i'(t)>0 \\
    0 & h_i'(t)<0 \\
    z_i(t) & h_i'(t)=0
\end{cases}
```

```math
E(t) = 
-\frac{1}{2}\sum_{i,j=1}^{N} w_{ij}(2z_i(t)-1)(2z_j(t)-1)=
```
```math
=-\frac{1}{2}
\left(
    4\sum_{i,j=1}^{N} w_{ij} z_i(t)z_j(t)
    -2\sum_{i,j=1}^{N} w_{ij}z_j(t)
    -2\sum_{i,j=1}^{N} w_{ij}z_i(t)
    +\sum_{i,j=1}^{N} w_{ij}
\right)=
```
```math
=4 \left(
    -\frac{1}{2}\sum_{i,j=1}^{N} w_{ij} z_i(t)z_j(t)
    +\frac{1}{2}\sum_{i,j=1}^{N} w_{ij}z_j(t)
    -\frac{1}{8}\sum_{i,j=1}^{N} w_{ij}
\right)=
```
```math
=4\left(
    -\frac{1}{2}\sum_{i,j=1}^{N} w_{ij} z_i(t)z_j(t)
    +\sum_{i,j=1}^{N}z_j(t)\frac{1}{2}w_{ij}
    -\frac{1}{8}\sum_{i,j=1}^{N} w_{ij}
\right)=
```
```math
=4\left(
    -\frac{1}{2}\sum_{i,j=1}^{N} w_{ij} z_i(t)z_j(t)
    +\sum_{i,j=1}^{N}b_jz_j(t)
    -\frac{1}{8}\sum_{i,j=1}^{N} w_{ij}
\right)
```

```math
\varepsilon(t)=
\frac{E(t)}{4}
+\frac{1}{8}\sum_{i,j=1}^{N} w_{ij}
```
```math
\varepsilon(t)=
-\frac{1}{2}\sum_{i,j=1}^{N} w_{ij} z_i(t)z_j(t)+\sum_{i,j=1}^{N}b_jz_j(t)
```

Если удастся подогнать задачу под энергетический функционал сети Хопфилда.