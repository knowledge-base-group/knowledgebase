### Билет 18. 

#### Интерфейс с внешним окружением в системе G2 (главные возможности). 

> [Man по G2](http://www.ntpdubna.ru/faq.shtml)

> Книга Рыбы и Паронджанова из следующего сема пригодится

В G2 реализована *распределённая обработка приложения* на принципах клиент-серверной архитектуры. Это позволяет совместно использовать данные, знания и задачи на различны платформах.

Для параллельной работы предусмотрен интерфейс *Telewindows*, позволяющий нескольким пользователям одновременно работать с одним приложением G2. Более того, этот режим отображает по-своему для каждого клиента, поэтому нет проблем с языком интерфейса приложения.

Работает G2 на винде, линуксах, бсд и других древностях. Созданное на G2 приложение получается *кроссплатформенным*.

Разработчик приложений G2 может создавать различные формы отчётов для выдачи во внешнюю систему, используя:
* экспорт файлов и данных
* отображение текста и графики в пользовательском интерфейсе Telewindows
* взаимодействие с приложениями через мосты G2-ActiveXLink, G2-JavaLink и т.п.
* взаимодействие с браузерами через G2-WebLink

Для случаев, когда мост не предусмотрен, G2 имеет несколько интерфейсов, позволяющих разработчикам создавать двусторонние связи с внешними источниками данных. Например, с помощью сервера данных *G2 Gateway Standard Interface (GSI)* разработчик приложений G2 может создавать интерфейсы ко внешним базам данных и системам.

GSI работает *параллельно* с G2, поэтому рассуждение в G2 может продолжаться и во время приема данных реального времени.

В качестве *основных функций* GSI можно перечислить следующие:
* получение данных из внешних источников
* управление значениями во внешних системах
* передача текстовых сообщений и получение подтверждений
* обработка событий на внешних серверах данных
* выполнение удаленных вызовов процедур (RPC) в любом
направлении

#### ИДС: основные понятия и определения.

*Общение* (коммуникативное воздействие, диалог) - процесс достижения его участниками определённых согласованных целей путём обмена связанными высказываниями, выраженными в языке о некотором реальном или гипотетическом мире (ПрО).

*Дискурс* - связный текст, составленный из высказываний участников общения. Связность дискурса достигается
* лингвистическими средствами
    * родо-видовыми
    * анафорическими
    * модальными
    * стилистическими согласованиями
    * согласованиями пресуппозиций
* экстралигвистическими средствами
    * временными связями
    * причинно-следственными связями

Цели, преследуемые участниками общения, определяют *структуру диалога*, которая рассматривается на трёх уровнях:
1. *Глобальная* структура диалога определяется общими свойствами решаемых пользователями задач.
2. *Тематическая* структура диалога зависит от конкретных особенностей решаемой задачи, т.е. от алгоритма её решения (разбиения задачи на подзадачи) и распределения функций между участниками общения.
3. *Локальная* структура диалога сводится к рассмотрению шагов диалога. 

*Шаг диалога* - пара "действие-реакция", где высказывание активного участника называется действием, а пассивного - реакцией. 

Основными параметрами на локальном уровне диалога являются
* инициатор шага и вид действия
* способ влияния действия на реакцию
* спецификация задачи

*Перехват инициативы* возникает тогда, когда пассивный участник вместо преследования цели, предложенной активным участником, предлагает преследовать иные цели. В частности, цели, способствующие преодолению локальных неудач.

Перехват инициативы прерывает текущий шаг диалога и открывает поддиалог, где ранее пассивный участник становится активным.