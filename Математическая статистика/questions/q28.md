#### 28. Задачи регрессионного анализа. Постановка задачи оценивания параметров регрессионной модели. Метод наименьших квадратов. Простейшая линейная регрессионная модель. Расчёт точечных оценок параметров модели. Интервальные оценки параметров (без вывода) и функции регрессии. Проверка значимости простейшей линейной регрессионной модели. Графическая иллюстрация.

**Задачи регрессионного анализа:**
1. Выбор класса функций для описания зависимости откликов модели $`Y_1,...,Y_l`$ от регрессоров $`X_1,...,X_m`$.
2. Нахождение оценок неизвестных параметров функции, описывающей зависимость откликов модели от регрессоров.
3. Статистический анализ найденной зависимости.
4. Предсказание значений откликов модели по результатам наблюдения регрессоров на основе найденной зависимости.

**Метод наименьших квадратов.**
Пусть $`\varphi(x, \beta_0,...,\beta_k)`$ - функция регрессии.

<img src="../images/q28_1.jpg" width=500>

Смысл метода наименьших квадратов состоит в следующем выражении:
```math
\tilde{D}_{\text{ост}} = \frac{1}{n} \sum\limits_{i=1}^n (y_i-\varphi(x, \beta_0,...,\beta_k))^2 \to \min\limits_{\beta_0,...,\beta_k}
```

Если взять простейшую линейную регрессию, то получим:
```math
\varphi(x) = \beta_0 + \beta_1x
```
Тогда формула становится меньше:
```math
\tilde{D}_{\text{ост}} = \frac{1}{n} \sum\limits_{i=1}^n (y_i-\beta_0-\beta_1x_i)^2 \to \min\limits_{\beta_0,\beta_1}
```
Находим экстремум:
```math
\begin{cases}
    \frac{\partial \tilde{D}_{\text{ост}}}{\partial \beta_0} = \frac{-2}{n} \sum\limits_{i=1}^n (y_i-\beta_0-\beta_1x_i) = 0 \\

    \frac{\partial \tilde{D}_{\text{ост}}}{\partial \beta_1} = \frac{-2}{n} \sum\limits_{i=1}^n x_i(y_i-\beta_0-\beta_1x_i) = 0
\end{cases}
```
```math
\begin{cases}
    \beta_0+\bar{x}\beta_1 = \bar{y} \\
    \beta_0\bar{x}+\frac{1}{n} \sum\limits_{i=1}^n x_i^2\beta_1 = \frac{1}{n} \sum\limits_{i=1}^n x_iy_i
\end{cases}
```
Получили точечные оценки параметров регрессии:
```math
\begin{cases}
    \beta_0=\bar{y}-\bar{x}\beta_1 = \bar{y}-\bar{x}\tilde{\rho}\frac{\tilde{\sigma}_Y}{\tilde{\sigma}_X} \\
    \beta_1 = \tilde{\rho}\frac{\tilde{\sigma}_Y}{\tilde{\sigma}_X}
\end{cases}
```

**Доверительные интервалы.**
* Для $`\beta_0`$:
```math
\tilde{\beta}_0 \pm t_{1-\alpha/2}(n-2) \sqrt{\tilde{D}_{\text{ост}}^{\text{несм}}}
    \sqrt{
        \frac
        {
            \sum\limits_{i=1}^n x_i^2
        }
        {
            n^2D_X^*
        }
    }
```
* Для $`\beta_1`$:
```math
\tilde{\beta}_1 \pm t_{1-\alpha/2}(n-2) \sqrt{\tilde{D}_{\text{ост}}^{\text{несм}}}
    \sqrt{
        \frac{1}{nD_X^*}
    }
```
* Для $`\varphi(x)`$:
```math
\tilde{\beta}_0 + \tilde{\beta}_1x \pm t_{1-\alpha/2}(n-2) \sqrt{\tilde{D}_{\text{ост}}^{\text{несм}}}
    \sqrt{
        \frac{1}{n}+
        \frac{(x-\bar{x})^2}{nD_X^*}
    }
```

**Замечание:**
```math
\tilde{D}_{\text{ост}}^{\text{несм}} = \frac{1}{n-2}
    \sum\limits_{i=1}^n (\varphi(x_i)-y_i)^2
```

**Утверждение 1.** Регрессионная модель значима, если $`\beta_1 \ne 0`$.

**Проверка значимости простейшей регрессионной модели.**
* $`H_0:\beta_1 = 0`$
* $`H':\beta_1 \ne 0`$

```math
Z = \frac{\tilde{D}_{\text{регр}}/1}{\tilde{D}_{\text{ост}}/(n-2)}
```
```math
Z|_{H_0} \sim F(1,n-2)
```
Критическая область правосторонняя.